FROM node
WORKDIR /var/app
COPY ./app /var/app
RUN npm install
CMD npm start